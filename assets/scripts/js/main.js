// import global library dependencies
import $ from 'jquery'


// import JS modules to compile
import './modules/carousel'
import './modules/headerScrollAnimations'
import './modules/popmotionAnimations'
import './modules/test'
import './modules/video'