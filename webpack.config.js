var path = require('path');
var glob = require('glob');

module.exports = {
    entry:   './assets/scripts/js/main.js',
    output: { 
      path: path.resolve(__dirname, './dist'),  
      filename: "main.js",
    },
    mode: 'none',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'es2015', 'stage-3']
                    }
                }
            }
        ]
    },
    node: {
      fs: "empty"
    },
}